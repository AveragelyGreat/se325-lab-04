package se325.lab04.concert.services;

import org.jboss.resteasy.annotations.jaxrs.PathParam;
import se325.lab04.concert.domain.Concert;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("concerts")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class ConcertResource {

    @GET
    @Path("{id}")
    public Response retrieveConcert(@PathParam("id") long id) {
        // Acquire an EntityManager (creating a new persistence context).
        EntityManager em = PersistenceManager.instance().createEntityManager();
        try {

            // Start a new transaction.
            em.getTransaction().begin();

            // Use the EntityManager to retrieve, persist or delete object(s).
            // Use em.find(), em.persist(), em.merge(), etc...
            Concert concert = em.find(Concert.class, id);

            // Commit the transaction.
            em.getTransaction().commit();

            if (concert == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            return Response.ok(concert).build();

        } finally {
            // When you're done using the EntityManager, close it to free up resources.
            em.close();
        }
    }

    @POST
    public Response createConcert(Concert concert) {
        // Acquire an EntityManager (creating a new persistence context).
        EntityManager em = PersistenceManager.instance().createEntityManager();
        try {

            // Start a new transaction.
            em.getTransaction().begin();

            // Use the EntityManager to retrieve, persist or delete object(s).
            // Use em.find(), em.persist(), em.merge(), etc...
            em.persist(concert);

            // Commit the transaction.
            em.getTransaction().commit();

            return Response.created(URI.create("/concerts/" + concert.getId())).build();

        } finally {
            // When you're done using the EntityManager, close it to free up resources.
            em.close();
        }
    }

    @PUT
    public Response updateConcert(Concert concert) {
        // Acquire an EntityManager (creating a new persistence context).
        EntityManager em = PersistenceManager.instance().createEntityManager();
        try {

            // Start a new transaction.
            em.getTransaction().begin();

            // Use the EntityManager to retrieve, persist or delete object(s).
            // Use em.find(), em.persist(), em.merge(), etc...
            em.merge(concert);

            // Commit the transaction.
            em.getTransaction().commit();

            return Response.status(204).build();

        } finally {
            // When you're done using the EntityManager, close it to free up resources.
            em.close();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deleteConcert(@PathParam("id") long id) {
        // Acquire an EntityManager (creating a new persistence context).
        EntityManager em = PersistenceManager.instance().createEntityManager();
        try {

            // Start a new transaction.
            em.getTransaction().begin();

            // Use the EntityManager to retrieve, persist or delete object(s).
            // Use em.find(), em.persist(), em.merge(), etc...
            Concert concert = em.find(Concert.class, id);
            em.getTransaction().commit();

            em.getTransaction().begin();
            em.remove(concert);

            // Commit the transaction.
            em.getTransaction().commit();

            return Response.status(204).build();

        } finally {
            // When you're done using the EntityManager, close it to free up resources.
            em.close();
        }
    }

    @DELETE
    public Response deleteAllConcerts() {
        // Acquire an EntityManager (creating a new persistence context).
        EntityManager em = PersistenceManager.instance().createEntityManager();
        try {

            // Start a new transaction.
            em.getTransaction().begin();

            // Use the EntityManager to retrieve, persist or delete object(s).
            // Use em.find(), em.persist(), em.merge(), etc....
            Query cq = em.createQuery("delete from Concert concert");
            cq.executeUpdate();

            // Commit the transaction.
            em.getTransaction().commit();

            return Response.status(204).build();

        } finally {
            // When you're done using the EntityManager, close it to free up resources.
            em.close();
        }
    }
}
